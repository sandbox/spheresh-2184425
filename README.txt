Extend Nodequeue functionality by adding 
a new vertical tab with dialog module in node edit form. 

This way you can add and remove nodes from queues without visiting Nodequeue tab.
